import { Injectable } from "@angular/core";
import { Book } from "./book.model";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class BookService {
  formData: Book;
  list: Book[];

  readonly BASE_URL = environment.API_URL + "/Book/";

  constructor(private http: HttpClient) {}

  refreshList() {
    this.http
      .get(this.BASE_URL)
      .toPromise()
      .then(res => {
        this.list = res as Book[];
      });
  }

  find() {
    return this.http.get(this.BASE_URL);
  }

  postBook(formData: Book) {
    return this.http.post(this.BASE_URL, formData);
  }

  putBook(formData: Book) {
    const headers = new HttpHeaders().set("Content-Type", "application/json");

    return this.http.put(this.BASE_URL + formData.id, formData, { headers });
  }

  deleteBook(id: string) {
    return this.http.delete(this.BASE_URL + id);
  }
}
