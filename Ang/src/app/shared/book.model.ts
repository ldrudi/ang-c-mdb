export class Book {
    id: string;
    title: string;
    publisher: string;
    price: number;
}
