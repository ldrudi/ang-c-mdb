import { Component, OnInit } from "@angular/core";
import { BookService } from "src/app/shared/book.service";
import { NgForm } from "@angular/forms";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-book",
  templateUrl: "./book.component.html",
  styleUrls: ["./book.component.css"]
})
export class BookComponent implements OnInit {
  constructor(public service: BookService, private toastr: ToastrService) {}

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form?: NgForm) {
    if (form != null) {
      form.resetForm();
    }
    this.service.formData = {
      id: null,
      title: "",
      publisher: "",
      price: null
    };
  }

  onSubmit(form: NgForm) {
    if (form.value.id == null) {
      this.insertRecord(form);
    } else {
      this.updateRecord(form);
    }
  }

  insertRecord(form: NgForm) {
    this.service.postBook(form.value).subscribe(res => {
      this.toastr.success("Added successfully!", "Book Admin.");
      this.resetForm(form);
      this.service.refreshList();
    });
  }

  updateRecord(form: any) {
    this.service.putBook(this.service.formData).subscribe(res => {
      this.toastr.info("Updated successfully!", "Book Admin.");
      this.resetForm(form);
      this.service.refreshList();
    });
  }
}
