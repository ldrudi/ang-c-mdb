﻿using MongoDB.Bson;
using System.Collections.Generic;
using System.Web.Http;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    [Route("api/Book")]
    public class BookController : ApiController
    {
        private  DataAccess _bookService = new DataAccess();

        // GET: api/<controller>
        [HttpGet]
        public IEnumerable<Book> GetAll()
        {
            IEnumerable<Book> books = _bookService.GetAllBooks();

            return books;
        }

        [HttpGet]
        [Route("api/Book/{id}/")]
        // GET api/<controller>/5
        public IHttpActionResult GetBook(string id)
        {
            Book book = _bookService.GetBook(new ObjectId(id));
            if (book == null)
            {
                return NotFound();
            }

            return Ok(book);
        }
        
        [HttpPost]
        // POST api/<controller>
        public IHttpActionResult Post([FromBody]Book book)
        {
            //ModelState.IsValid()
            _bookService.Create(book);
            return CreatedAtRoute("DefaultApi", new { controller = "Book", id = book.Id }, book);
        }

        // PUT api/<controller>/5
        [HttpPut]
        [Route("api/Book/{id}/")]
        public IHttpActionResult Put(string id, [FromBody]Book book)
        {
            ObjectId recId = new ObjectId(id);
            Book b = _bookService.GetBook(recId);
            if (b == null)
            {
                return NotFound();
            }

            _bookService.Update(recId, book);
            return Ok(book);
        }

        // DELETE api/<controller>/5
        
        [HttpDelete]
        [Route("api/Book/{id}/")]
        public IHttpActionResult Delete(string id)
        {
            Book book = _bookService.GetBook(new ObjectId(id));
            if (book == null)
            {
                return NotFound();
            }

            _bookService.Remove(book.Id);
            return Ok(book);
        }

    }
}
