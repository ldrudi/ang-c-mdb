﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;

namespace WebApplication.Models
{
    public class DataAccess
    {
        private readonly IMongoCollection<Book> _books;

        public DataAccess()
        {
            MongoClient _client = new MongoClient("mongodb://localhost:27017");
            IMongoDatabase _db = _client.GetDatabase("bookStore");
            _books = _db.GetCollection<Book>("Books");
        }

        public IEnumerable<Book> GetAllBooks()
        {
            return _books.Find(_ => true).ToList();
        }
        
        public Book GetBook(ObjectId id)
        {
            return _books.Find(_ => _.Id == id).Single();
        }

        public Book Create(Book b)
        {
            _books.InsertOne(b);
            return b;
        }

        public void Update(ObjectId id, Book b)
        {
            b.Id = id;
            var filter = Builders<Book>.Filter.Eq(bd => bd.Id, id);
            _books.ReplaceOne(filter, b);
        }

        public void Remove(ObjectId id)
        {
            DeleteResult operation = _books.DeleteOne(e => e.Id == id);
        }
    }
}
