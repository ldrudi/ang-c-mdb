﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace WebApplication.Models
{
    public class Book
    {
        //[BsonId]
        public ObjectId Id { get; set; }

        //[BsonElement("title")]
        public string Title { get; set; }

        //[BsonElement("publisher")]
        public string Publisher { get; set; }

        //[BsonElement("price")]
        public int Price { get; set; }

        [BsonExtraElements]
        public BsonDocument Metadata { get; set; }
    }
}